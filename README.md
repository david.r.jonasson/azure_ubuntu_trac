# Trac on Ubuntu on Azure

## Pre-req
First create an account on Azure, see: https://azure.microsoft.com/en-us/

Install Azure CLI, see: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

Install Packer, see: https://packer.io/

## Create resource group
Login to your Azure account:

`az login`

Now use the Azure CLI to create the resource group you want. For example:

`az group create --name myresourcegroup --location "West Europe"`

## Create the image
First you need to set these environment variables:

ARM_CLIENT_ID

ARM_CLIENT_SECRET

ARM_SUBSCRIPTION_ID

Like:
`export ARM_CLIENT_ID="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"`

Ensure the resource group also is set in the `ubuntu_trac.json` file.

Also you need the location to be the same as when you created the resource group.

Create the image:

`packer build ubuntu_trac.json`

## Use
After creating the VM, open a browser and go to `[ip]/trac/[PROJECT_NAME]` and log in.
