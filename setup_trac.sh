#!/usr/bin/env bash

ADMIN_PASSWORD="ChangeMe"

PROJECT_NAME="mytrac"
echo "Creating Trac Project " $PROJECT_NAME

## Install Apache
apt-get install apache2 -y

## Install Trac
apt-get install trac libapache2-mod-wsgi -y

## Start Apache service and enable start on boot
systemctl start apache2
systemctl enable apache2

## Enable auth_digest module
a2enmod auth_digest

## Create a web root directory for Trac
mkdir /var/lib/trac
mkdir -p /var/www/html/trac
chown www-data:www-data /var/www/html/trac

## Create a project directory for Trac and give proper permissions
trac-admin /var/lib/trac/$PROJECT_NAME initenv $PROJECT_NAME sqlite:db/trac.db
trac-admin /var/lib/trac/$PROJECT_NAME deploy /var/www/html/trac/$PROJECT_NAME
chown -R www-data:www-data /var/lib/trac/$PROJECT_NAME
chown -R www-data:www-data /var/www/html/trac/$PROJECT_NAME

## Create admin user and guest user
printf $ADMIN_PASSWORD'\n'$ADMIN_PASSWORD'\n' | htdigest -c /var/lib/trac/$PROJECT_NAME/.htdigest $PROJECT_NAME admin
trac-admin /var/lib/trac/$PROJECT_NAME permission add admin TRAC_ADMIN
printf 'guest\nguest\n' | htdigest /var/lib/trac/$PROJECT_NAME/.htdigest $PROJECT_NAME guest

## Create Apache virtual host directive for Trac
cat >/etc/apache2/sites-available/trac.conf <<EOL
WSGIScriptAlias /trac/$PROJECT_NAME /var/www/html/trac/$PROJECT_NAME/cgi-bin/trac.wsgi
<Location /trac/$PROJECT_NAME>
  AuthType Digest
  AuthName '$PROJECT_NAME'
  AuthUserFile /var/lib/trac/$PROJECT_NAME/.htdigest
  Require valid-user
</Location>
EOL

## Custom edits of trac
# Remove text about missing logo
sed -i "26s/.*/alt =/" /var/lib/trac/testlab/conf/trac.ini
# Set English as language
sed -i "214s/.*/default_language = en_US/" /var/lib/trac/testlab/conf/trac.ini

## Restart Apache service
sudo a2ensite trac.conf
systemctl restart apache2
